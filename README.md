# ssg-custom

Tools for use with Roman Zolotarev's [ssg
script](https://www.romanzolotarev.com/ssg.html) + his scripts.

To use the buildsite tool for deploying a static site to a remote host:

1. Copy ```buildsite```, ```ssg3```, and ```rssg``` to ```~/bin```
2. Copy ```buildsite.conf``` to a convenient location and fill it out for your website.
3. Run ```buildsite website.conf```.
